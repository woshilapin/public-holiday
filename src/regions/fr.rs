use crate::{Date, FromDate, PublicHoliday};

/// Describe public holidays for France.
#[derive(Debug, PartialEq)]
pub enum JourFerie {
    /// New Year of the current year, on the 1st of January
    NouvelAn,
    /// The Monday after Easter
    LundiDePaques,
    /// Christmas Day, on the 25th of December
    Noel,
}

use JourFerie::*;
impl PublicHoliday for JourFerie {
    fn from_ymd(year: i32, month: u32, day: u32) -> Option<Self> {
        if let Some(date) = crate::easter::easter_monday_from_year::<Date>(year) {
            if date.1 == month && date.2 == day {
                return Some(LundiDePaques);
            }
        }
        match month {
            1 => match day {
                1 => Some(NouvelAn),
                _ => None,
            },
            12 => match day {
                25 => Some(Noel),
                _ => None,
            },
            _ => None,
        }
    }
    fn in_year<D>(&self, year: i32) -> Option<D>
    where
        D: FromDate,
    {
        match self {
            NouvelAn => Some(D::from_ymd(year, 1, 1)),
            LundiDePaques => crate::easter::easter_monday_from_year(year),
            Noel => Some(D::from_ymd(year, 12, 25)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Date;

    mod from_ymd {
        use super::*;

        #[test]
        fn nouvel_an() {
            let holiday = JourFerie::from_ymd(1985, 1, 1).unwrap();
            assert_eq!(holiday, NouvelAn);
        }

        #[test]
        fn lundi_de_paques() {
            let holiday = JourFerie::from_ymd(1985, 4, 8).unwrap();
            assert_eq!(holiday, LundiDePaques);
        }

        #[test]
        fn noel() {
            let holiday = JourFerie::from_ymd(1985, 12, 25).unwrap();
            assert_eq!(holiday, Noel);
        }

        #[test]
        fn none() {
            let holiday = JourFerie::from_ymd(1985, 12, 10);
            assert_eq!(holiday, None);
        }
    }

    mod in_year {
        use super::*;

        #[test]
        fn nouvel_an() {
            let nouvel_an = NouvelAn;
            let date = nouvel_an.in_year::<Date>(1985).unwrap();
            assert_eq!(date.0, 1985);
            assert_eq!(date.1, 1);
            assert_eq!(date.2, 1);
        }

        #[test]
        fn lundi_de_paques() {
            let lundi_de_paques = LundiDePaques;
            let date = lundi_de_paques.in_year::<Date>(1985).unwrap();
            assert_eq!(date.0, 1985);
            assert_eq!(date.1, 4);
            assert_eq!(date.2, 8);
        }

        #[test]
        fn noel() {
            let noel = Noel;
            let date = noel.in_year::<Date>(1985).unwrap();
            assert_eq!(date.0, 1985);
            assert_eq!(date.1, 12);
            assert_eq!(date.2, 25);
        }
    }
}
