use crate::{easter, Date, FromDate, PublicHoliday, Weekday};

/// Describe public holidays for United Kingdom.
#[derive(Debug, PartialEq)]
pub enum BankHoliday {
    /// New Year of the current year, on the 1st of January
    NewYear,
    /// The Monday after Easter
    EasterMonday,
    /// Christmas Day, on the 25th of December
    Christmas,
    /// Boxing Day, usually the 26th of December, but moved to Monday when on a
    /// Weekend
    BoxingDay,
}

use BankHoliday::*;
impl PublicHoliday for BankHoliday {
    fn from_ymd(year: i32, month: u32, day: u32) -> Option<Self> {
        if let Some(date) = easter::easter_monday_from_year::<Date>(year) {
            if date.1 == month && date.2 == day {
                return Some(EasterMonday);
            }
        }
        if let Some(date) = christmas_from_year::<Date>(year) {
            if date.1 == month && date.2 == day {
                return Some(Christmas);
            }
        }
        if let Some(date) = boxing_day_from_year::<Date>(year) {
            if date.1 == month && date.2 == day {
                return Some(BoxingDay);
            }
        }
        if month == 1 {
            if let Some(weekday) = crate::weekday::weekday_from(year, month, 1) {
                if (weekday == Weekday::Saturday && day == 3)
                    || (weekday == Weekday::Sunday && day == 2)
                    || day == 1
                {
                    return Some(NewYear);
                }
            }
        }
        None
    }
    fn in_year<D>(&self, year: i32) -> Option<D>
    where
        D: FromDate,
    {
        match self {
            NewYear => crate::weekday::weekday_from(year, 1, 1).map(|wd| {
                if wd == Weekday::Saturday {
                    D::from_ymd(year, 1, 3)
                } else if wd == Weekday::Sunday {
                    D::from_ymd(year, 1, 2)
                } else {
                    D::from_ymd(year, 1, 1)
                }
            }),
            EasterMonday => easter::easter_monday_from_year(year),
            Christmas => Some(D::from_ymd(year, 12, 25)),
            BoxingDay => boxing_day_from_year(year),
        }
    }
}

// Boxing Day is the 26th of December.
// However, the Public Holiday corresponding to Boxing Day might be moved to the
// following Monday or Tuesday if the 26th falls respectively on a Saturday or a
// Sunday.
// https://en.wikipedia.org/wiki/Boxing_Day#Status_by_country
fn boxing_day_from_year<D>(year: i32) -> Option<D>
where
    D: FromDate,
{
    crate::weekday::weekday_from(year, 12, 26).map(|boxing_day_weekday| {
        let boxing_day = match boxing_day_weekday {
            Weekday::Saturday | Weekday::Sunday => 28,
            _ => 26,
        };
        D::from_ymd(year, 12, boxing_day)
    })
}

// Christmas is the 25th of December.
// However, the Public Holiday corresponding to Christmas might be moved to the
// following Monday or Tuesday if the 25th falls respectively on a Saturday or a
// Sunday.
// https://en.wikipedia.org/wiki/Boxing_Day#Status_by_country
fn christmas_from_year<D>(year: i32) -> Option<D>
where
    D: FromDate,
{
    crate::weekday::weekday_from(year, 12, 25).map(|christmas_weekday| {
        let christmas_day = match christmas_weekday {
            Weekday::Saturday | Weekday::Sunday => 27,
            _ => 25,
        };
        D::from_ymd(year, 12, christmas_day)
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Date;

    #[test]
    fn from_ymd() {
        assert_eq!(BankHoliday::from_ymd(1985, 1, 1), Some(NewYear));
        assert_eq!(BankHoliday::from_ymd(2011, 1, 3), Some(NewYear));
        assert_eq!(BankHoliday::from_ymd(2017, 1, 2), Some(NewYear));
        assert_eq!(BankHoliday::from_ymd(1985, 4, 8), Some(EasterMonday));
        assert_eq!(BankHoliday::from_ymd(1985, 12, 25), Some(Christmas));
        assert_eq!(BankHoliday::from_ymd(2019, 12, 26), Some(BoxingDay));
        assert_eq!(BankHoliday::from_ymd(1985, 12, 10), None);
    }

    mod in_year {
        use super::*;

        #[test]
        fn new_year() {
            let date = NewYear.in_year::<Date>(1985).unwrap();
            assert_eq!(date.0, 1985);
            assert_eq!(date.1, 1);
            assert_eq!(date.2, 1);
            // Substitute New Year Saturday
            let date = NewYear.in_year::<Date>(2011).unwrap();
            assert_eq!(date.0, 2011);
            assert_eq!(date.1, 1);
            assert_eq!(date.2, 3);
            // Substitute New Year Sunday
            let date = NewYear.in_year::<Date>(2017).unwrap();
            assert_eq!(date.0, 2017);
            assert_eq!(date.1, 1);
            assert_eq!(date.2, 2);
        }

        #[test]
        fn easter_monday() {
            let date = EasterMonday.in_year::<Date>(1935).unwrap();
            assert_eq!(date.0, 1935);
            assert_eq!(date.1, 4);
            assert_eq!(date.2, 22);
            // Before Gregorian Calendar
            let date = EasterMonday.in_year::<Date>(1582);
            assert_eq!(date, None);
        }

        #[test]
        fn christmas() {
            let date = Christmas.in_year::<Date>(1985).unwrap();
            assert_eq!(date.0, 1985);
            assert_eq!(date.1, 12);
            assert_eq!(date.2, 25);
        }

        #[test]
        fn boxing_day() {
            let date = BoxingDay.in_year::<Date>(2019).unwrap();
            assert_eq!(date.0, 2019);
            assert_eq!(date.1, 12);
            assert_eq!(date.2, 26);
        }
    }

    #[test]
    fn christmas() {
        let date = christmas_from_year::<Date>(2019).unwrap();
        assert_eq!(date.0, 2019);
        assert_eq!(date.1, 12);
        assert_eq!(date.2, 25);
        // Substitute Christmas Saturday
        let date = christmas_from_year::<Date>(2010).unwrap();
        assert_eq!(date.0, 2010);
        assert_eq!(date.1, 12);
        assert_eq!(date.2, 27);
        // Substitute Christmas Sunday
        let date = christmas_from_year::<Date>(2016).unwrap();
        assert_eq!(date.0, 2016);
        assert_eq!(date.1, 12);
        assert_eq!(date.2, 27);
        // Before Gregorian Calendar
        let date = christmas_from_year::<Date>(1582);
        assert_eq!(date, None);
    }

    #[test]
    fn boxing_day() {
        let date = boxing_day_from_year::<Date>(2019).unwrap();
        assert_eq!(date.0, 2019);
        assert_eq!(date.1, 12);
        assert_eq!(date.2, 26);
        // Substitue Boxing Day Saturday
        let date = boxing_day_from_year::<Date>(2020).unwrap();
        assert_eq!(date.0, 2020);
        assert_eq!(date.1, 12);
        assert_eq!(date.2, 28);
        // Substitue Boxing Day Sunday
        let date = boxing_day_from_year::<Date>(2010).unwrap();
        assert_eq!(date.0, 2010);
        assert_eq!(date.1, 12);
        assert_eq!(date.2, 28);
        // Before Gregorian Calendar
        let date = boxing_day_from_year::<Date>(1582);
        assert_eq!(date, None);
    }
}
