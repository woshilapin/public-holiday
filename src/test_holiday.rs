use crate::{FromDate, PublicHoliday};

#[derive(Debug, PartialEq)]
pub enum TestHoliday {
    BeforeZero,
    AfterZero,
}

use TestHoliday::*;
impl PublicHoliday for TestHoliday {
    fn from_ymd(year: i32, month: u32, day: u32) -> Option<Self> {
        if month == 1 && day == 1 {
            if year < 0 {
                return Some(BeforeZero);
            } else if year > 0 {
                return Some(AfterZero);
            }
        }
        None
    }

    fn in_year<D>(&self, year: i32) -> Option<D>
    where
        D: FromDate,
    {
        if year < 0 && *self == BeforeZero {
            Some(D::from_ymd(year, 1, 1))
        } else if year > 0 && *self == AfterZero {
            Some(D::from_ymd(year, 1, 1))
        } else {
            None
        }
    }
}
