use crate::{FromDate, IsHoliday, PublicHoliday};

#[derive(Debug, PartialEq)]
pub struct Date(pub i32, pub u32, pub u32);

impl FromDate for Date {
    fn from_ymd(year: i32, month: u32, day: u32) -> Self {
        Date(year, month, day)
    }
}

impl IsHoliday for Date {
    fn is_holiday<H>(&self) -> Option<H>
    where
        H: PublicHoliday,
    {
        H::from_ymd(self.0, self.1, self.2)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_holiday::TestHoliday;

    #[test]
    fn from_ymd() {
        let date = Date::from_ymd(1985, 1, 1);
        assert_eq!(date.0, 1985);
        assert_eq!(date.1, 1);
        assert_eq!(date.2, 1);
    }

    #[test]
    fn is_holiday() {
        let date = Date(1985, 1, 1);
        assert_eq!(date.is_holiday(), Some(TestHoliday::AfterZero));
    }
}
