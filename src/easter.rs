use crate::{utils::DAYS_PER_MONTH, Date, FromDate};
use std::convert::TryFrom;

/// Calculate Easter's date for the specified year.
/// Based on the algorithm detailed in
/// https://www.wikiwand.com/fr/Calcul_de_la_date_de_P%C3%A2ques
pub fn easter_from_year<D>(year: i32) -> Option<D>
where
    D: FromDate,
{
    if year < 1583 {
        return None;
    }
    let meton_cycle = year % 19;
    let year_hundred = year / 100;
    let year_rank = year % 100;
    let century = year_hundred / 4;
    let bissextile_century = year_hundred % 4;
    let proemptose_cycle = (year_hundred + 8) / 25;
    let proemptose = (year_hundred - proemptose_cycle + 1) / 3;
    let epacte = (19 * meton_cycle + year_hundred - century - proemptose + 15) % 30;
    let bissextile_year = year_rank / 4;
    let bissextile_year_remind = year_rank % 4;
    let dominical =
        (2 * bissextile_century + 2 * bissextile_year - epacte - bissextile_year_remind + 32) % 7;
    let correction = (meton_cycle + 11 * epacte + 22 * dominical) / 451;
    let result = epacte + dominical - 7 * correction + 114;
    let month = result / 31;
    let day = result % 31 + 1;
    Some(D::from_ymd(year, month as u32, day as u32))
}

/// Calculate the date of Easter Monday for the specified year.
pub fn easter_monday_from_year<D>(year: i32) -> Option<D>
where
    D: FromDate,
{
    easter_from_year::<Date>(year).map(|easter_date| {
        // Easter can be in March or April, up to the 25th of April
        // If Easter is the 31st of March, then Easter Monday needs to change the month.
        if easter_date.1 == 3 && easter_date.2 == 31 {
            D::from_ymd(easter_date.0, 4, 1)
        } else {
            D::from_ymd(easter_date.0, easter_date.1, easter_date.2 + 1)
        }
    })
}

pub fn days_from_easter(year: i32, month: u32, day: u32) -> Option<i32> {
    if let Some(easter_date) = crate::easter::easter_from_year::<Date>(year) {
        if month < easter_date.1 || month == easter_date.1 && day < easter_date.2 {
            // Current date is before easter for the current year
            let mut days = 0i32;
            for m in month..=easter_date.1 {
                days -= i32::try_from(*DAYS_PER_MONTH.get(&m).unwrap()).unwrap();
                if m == 2 && crate::utils::is_leap_year(year) {
                    days -= 1;
                }
                if m == month {
                    days += day as i32;
                }
                if m == easter_date.1 {
                    days += i32::try_from(*DAYS_PER_MONTH.get(&m).unwrap()).unwrap()
                        - i32::try_from(easter_date.2).unwrap();
                }
            }
            return Some(days);
        } else {
            // Current date is after easter for the current year
            let mut days = 0i32;
            for m in easter_date.1..=month {
                days += i32::try_from(*DAYS_PER_MONTH.get(&m).unwrap()).unwrap();
                if m == easter_date.1 {
                    days -= easter_date.2 as i32;
                }
                if m == month {
                    days -= i32::try_from(*DAYS_PER_MONTH.get(&m).unwrap()).unwrap()
                        - i32::try_from(day).unwrap();
                }
            }
            return Some(days);
        }
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Date;

    mod easter {
        use super::*;

        #[test]
        fn in_1500() {
            let date = easter_from_year::<Date>(1500);
            assert_eq!(date, None);
        }

        #[test]
        fn in_1961() {
            let date = easter_from_year(1961);
            assert_eq!(date, Some(Date(1961, 4, 2)));
        }

        #[test]
        fn in_2006() {
            let date = easter_from_year(2006);
            assert_eq!(date, Some(Date(2006, 4, 16)));
        }

        #[test]
        fn in_2019() {
            let date = easter_from_year(2019);
            assert_eq!(date, Some(Date(2019, 4, 21)));
        }
    }

    mod easter_monday {
        use super::*;

        #[test]
        fn easter_monday() {
            let date = easter_monday_from_year::<Date>(1935).unwrap();
            assert_eq!(date.0, 1935);
            assert_eq!(date.1, 4);
            assert_eq!(date.2, 22);
        }

        #[test]
        fn easter_monday_april_1() {
            let date = easter_monday_from_year::<Date>(1907).unwrap();
            assert_eq!(date.0, 1907);
            assert_eq!(date.1, 4);
            assert_eq!(date.2, 1);
        }
    }

    mod days_from_easter {
        use super::*;

        // Easter in 2019 was the 21st of April
        #[test]
        fn from_easter_2019() {
            assert_eq!(days_from_easter(2019, 1, 1), Some(-21 - 31 - 28 - 31 + 1));
            assert_eq!(days_from_easter(2019, 4, 20), Some(-1));
            assert_eq!(days_from_easter(2019, 4, 21), Some(0));
            assert_eq!(days_from_easter(2019, 4, 22), Some(1));
            assert_eq!(
                days_from_easter(2019, 12, 31),
                Some((30 - 21) + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31)
            );
        }

        // Easter in 2008 was the 23rd of March and 2008 was a leap year
        #[test]
        fn from_easter_2008() {
            assert_eq!(days_from_easter(2008, 1, 1), Some(-23 - 29 - 31 + 1));
            assert_eq!(days_from_easter(2008, 2, 29), Some(-23 - 1 + 1));
            assert_eq!(days_from_easter(2008, 3, 22), Some(-1));
            assert_eq!(days_from_easter(2008, 3, 23), Some(0));
            assert_eq!(days_from_easter(2008, 3, 24), Some(1));
            assert_eq!(
                days_from_easter(2008, 12, 31),
                Some((31 - 23) + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31)
            );
        }
    }
}
