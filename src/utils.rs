use lazy_static::lazy_static;
use std::collections::HashMap;

lazy_static! {
    pub static ref DAYS_PER_MONTH: HashMap<u32, u32> = {
        let mut map = HashMap::new();
        map.insert(1, 31);
        map.insert(2, 28);
        map.insert(3, 31);
        map.insert(4, 30);
        map.insert(5, 31);
        map.insert(6, 30);
        map.insert(7, 31);
        map.insert(8, 31);
        map.insert(9, 30);
        map.insert(10, 31);
        map.insert(11, 30);
        map.insert(12, 31);
        map
    };
}

#[cfg(feature = "easter")]
pub fn is_leap_year(year: i32) -> bool {
    year % 400 == 0 || year % 4 == 0 && year % 100 != 0
}

#[cfg(test)]
mod tests {
    #[cfg(feature = "easter")]
    #[test]
    fn leap_year() {
        assert_eq!(super::is_leap_year(2019), false);
        assert_eq!(super::is_leap_year(2008), true);
        assert_eq!(super::is_leap_year(1900), false);
        assert_eq!(super::is_leap_year(2000), true);
    }
}
