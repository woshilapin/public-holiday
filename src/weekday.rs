//! Describe the day of the week and offer some arithmetic on it.

use std::ops::{Add, Sub};

/// Enum for the seven days of the week.
///
/// Some arithmetic is also possible with this enum
/// ```
/// # use public_holiday::{Weekday, Weekday::*};
/// // Get the next day
/// assert_eq!(Monday.next(), Tuesday);
/// // Get the previous day
/// assert_eq!(Monday.previous(), Sunday);
/// // Add and/or substract days
/// assert_eq!(Monday + 5i32 - 7u32, Saturday);
/// ```
#[derive(Debug, Eq, Hash, PartialEq)]
pub enum Weekday {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
}

use Weekday::*;
impl Weekday {
    /// Give the next day in order:
    /// Monday->Tuesday->Wednesday->Thursday->Friday->Saturday->Sunday
    pub fn next(&self) -> Self {
        match self {
            Monday => Tuesday,
            Tuesday => Wednesday,
            Wednesday => Thursday,
            Thursday => Friday,
            Friday => Saturday,
            Saturday => Sunday,
            Sunday => Monday,
        }
    }

    /// Give the previous day in order:
    /// Sunday->Saturday->Friday->Thursday->Wednesday->Tuesday->Monday
    pub fn previous(&self) -> Self {
        match self {
            Monday => Sunday,
            Tuesday => Monday,
            Wednesday => Tuesday,
            Thursday => Wednesday,
            Friday => Thursday,
            Saturday => Friday,
            Sunday => Saturday,
        }
    }
}

impl Add<i32> for Weekday {
    type Output = Self;
    /// Find out the day of the week in `days` days
    fn add(self, days: i32) -> Self {
        let mut result = self;
        // '%' is arithmetic remainder, not modulo, so it might be negative
        for _ in 0..((days % 7 + 7) % 7) {
            result = result.next();
        }
        result
    }
}

impl Add<u32> for Weekday {
    type Output = Self;
    /// Find out the day of the week in `days` days
    fn add(self, days: u32) -> Self {
        let mut result = self;
        for _ in 0..((days % 7 + 7) % 7) {
            result = result.next();
        }
        result
    }
}

impl Sub<i32> for Weekday {
    type Output = Self;
    /// Find out the day of the week `days` days ago
    fn sub(self, days: i32) -> Self {
        let mut result = self;
        // '%' is arithmetic remainder, not modulo, so it might be negative
        for _ in 0..((days % 7 + 7) % 7) {
            result = result.previous();
        }
        result
    }
}

impl Sub<u32> for Weekday {
    type Output = Self;
    /// Find out the day of the week `days` days ago
    fn sub(self, days: u32) -> Self {
        let mut result = self;
        for _ in 0..((days % 7 + 7) % 7) {
            result = result.previous();
        }
        result
    }
}

#[cfg(feature = "easter")]
/// Calculate the weekday of a date
pub fn weekday_from(year: i32, month: u32, day: u32) -> Option<Weekday> {
    // Easter is always a Sunday.
    // With the number of days away from Easter (thanks to `days_from_easter`)
    // then from Sunday, we can iterate over the days to find out the weekday
    crate::easter::days_from_easter(year, month, day).map(|days| Weekday::Sunday.add(days))
}

#[cfg(test)]
mod tests {
    use super::*;

    mod next_previous {
        use super::*;
        use std::collections::HashMap;

        #[test]
        fn next() {
            let mut check_map = HashMap::new();
            check_map.insert(Monday, Tuesday);
            check_map.insert(Tuesday, Wednesday);
            check_map.insert(Wednesday, Thursday);
            check_map.insert(Thursday, Friday);
            check_map.insert(Friday, Saturday);
            check_map.insert(Saturday, Sunday);
            check_map.insert(Sunday, Monday);
            for (k, v) in check_map.drain() {
                assert_eq!(k.next(), v);
            }
        }

        #[test]
        fn previous() {
            let mut check_map = HashMap::new();
            check_map.insert(Monday, Sunday);
            check_map.insert(Tuesday, Monday);
            check_map.insert(Wednesday, Tuesday);
            check_map.insert(Thursday, Wednesday);
            check_map.insert(Friday, Thursday);
            check_map.insert(Saturday, Friday);
            check_map.insert(Sunday, Saturday);
            for (k, v) in check_map.drain() {
                assert_eq!(k.previous(), v);
            }
        }
    }

    mod add_sub {
        use super::*;

        #[test]
        fn add_i32() {
            assert_eq!(Monday + 2i32, Wednesday);
            assert_eq!(Monday + 9i32, Wednesday);
            assert_eq!(Sunday + 77i32, Sunday);
            // Shouldn't panic or overflow
            assert_eq!(Sunday + i32::min_value(), Friday);
            // Shouldn't panic or overflow
            assert_eq!(Sunday + i32::max_value(), Monday);
            assert_eq!(Monday + -2i32, Saturday);
        }

        #[test]
        fn add_u32() {
            assert_eq!(Monday + 2u32, Wednesday);
            assert_eq!(Monday + 9u32, Wednesday);
            assert_eq!(Sunday + 77u32, Sunday);
            assert_eq!(Sunday + u32::min_value(), Sunday);
            // Shouldn't panic or overflow
            assert_eq!(Sunday + u32::max_value(), Wednesday);
        }

        #[test]
        fn sub_i32() {
            assert_eq!(Monday - 2i32, Saturday);
            assert_eq!(Monday - 9i32, Saturday);
            assert_eq!(Sunday - 77i32, Sunday);
            // Shouldn't panic or overflow
            assert_eq!(Sunday - i32::min_value(), Tuesday);
            // Shouldn't panic or overflow
            assert_eq!(Sunday - i32::max_value(), Saturday);
            assert_eq!(Monday - -2i32, Wednesday);
        }

        #[test]
        fn sub_u32() {
            assert_eq!(Monday - 2u32, Saturday);
            assert_eq!(Monday - 9u32, Saturday);
            assert_eq!(Sunday - 77u32, Sunday);
            assert_eq!(Sunday - u32::min_value(), Sunday);
            // Shouldn't panic or overflow
            assert_eq!(Sunday - u32::max_value(), Thursday);
        }
    }

    #[cfg(feature = "easter")]
    #[test]
    fn calculate_weekday() {
        assert_eq!(weekday_from(2019, 1, 1), Some(Tuesday));
        assert_eq!(weekday_from(2008, 2, 29), Some(Friday));
        assert_eq!(weekday_from(1583, 1, 1), Some(Saturday));
        // Before Gregorian Calendar
        assert_eq!(weekday_from(1582, 1, 1), None);
    }
}
