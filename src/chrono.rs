use crate::{FromDate, IsHoliday, PublicHoliday};
use chrono::{Datelike, NaiveDate};

impl FromDate for NaiveDate {
    fn from_ymd(year: i32, month: u32, day: u32) -> Self {
        NaiveDate::from_ymd(year, month, day)
    }
}

impl IsHoliday for NaiveDate {
    fn is_holiday<H>(&self) -> Option<H>
    where
        H: PublicHoliday,
    {
        H::from_ymd(self.year(), self.month(), self.day())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    mod from_ymd {
        use super::*;

        #[test]
        fn in_year() {
            let date = NaiveDate::from_ymd(1985, 1, 1);
            assert_eq!(date.year(), 1985);
            assert_eq!(date.month(), 1);
            assert_eq!(date.day(), 1);
        }
    }

    mod is_holiday {
        use super::*;
        use crate::test_holiday::TestHoliday;

        #[test]
        fn is_none() {
            let date = NaiveDate::from_ymd(0, 1, 1);
            assert_eq!(date.is_holiday::<TestHoliday>(), None);
        }
    }
}
