//! This crate helps to work with holidays for a country.
//!
//! # Quick introduction
//! This crate is mainly defining the trait `PublicHoliday`.
//! This trait is providing a way to:
//! - find if a date is a public holiday
//! - find a specific public holiday in a specific year
//!
//! For example, let's implement that trait for France and only for New Year.
//! ```
//! # use public_holiday::{FromDate, PublicHoliday};
//! #[derive(Debug, PartialEq)]
//! enum FrancePublicHoliday {
//!   NewYear,
//! }
//!
//! impl PublicHoliday for FrancePublicHoliday {
//!   fn from_ymd(year: i32, month: u32, day: u32) -> Option<Self> {
//!     if month == 1 && day == 1 {
//!       Some(FrancePublicHoliday::NewYear)
//!     } else {
//!       None
//!     }
//!   }
//!   fn in_year<D: FromDate>(&self, year: i32) -> Option<D> {
//!     if *self == FrancePublicHoliday::NewYear {
//!       Some(D::from_ymd(year, 1, 1))
//!     } else {
//!       None
//!     }
//!   }
//! }
//! ```
//!
//! Now, you can find out if a specific date is a public holiday in France.
//! ```
//! # use public_holiday::{FromDate, PublicHoliday};
//! # #[derive(Debug, PartialEq)]
//! # enum FrancePublicHoliday { NewYear }
//! # impl PublicHoliday for FrancePublicHoliday {
//! #   fn from_ymd(year: i32, month: u32, day: u32) -> Option<Self> {
//! #     if month == 1 && day == 1 {
//! #       Some(FrancePublicHoliday::NewYear)
//! #     } else {
//! #       None
//! #     }
//! #   }
//! #   fn in_year<D: FromDate>(&self, year: i32) -> Option<D> {
//! #     if *self == FrancePublicHoliday::NewYear {
//! #       Some(D::from_ymd(year, 1, 1))
//! #     } else {
//! #       None
//! #     }
//! #   }
//! # }
//! let holiday = FrancePublicHoliday::from_ymd(1970, 1, 1);
//! assert_eq!(holiday, Some(FrancePublicHoliday::NewYear));
//!
//! let no_holiday = FrancePublicHoliday::from_ymd(1970, 1, 2);
//! assert_eq!(no_holiday, None);
//! ```
//!
//! You can also know the date of a specific public holiday for a specific year.
//! ```
//! # use public_holiday::{Date, FromDate, PublicHoliday};
//! # #[derive(Debug, PartialEq)]
//! # enum FrancePublicHoliday { NewYear }
//! # impl PublicHoliday for FrancePublicHoliday {
//! #   fn from_ymd(year: i32, month: u32, day: u32) -> Option<Self> {
//! #     if month == 1 && day == 1 {
//! #       Some(FrancePublicHoliday::NewYear)
//! #     } else {
//! #       None
//! #     }
//! #   }
//! #   fn in_year<D: FromDate>(&self, year: i32) -> Option<D> {
//! #     if *self == FrancePublicHoliday::NewYear {
//! #       Some(D::from_ymd(year, 1, 1))
//! #     } else {
//! #       None
//! #     }
//! #   }
//! # }
//! let date = FrancePublicHoliday::NewYear.in_year::<Date>(1985).unwrap();
//! assert_eq!(date, Date(1985, 1, 1));
//! ```
//!
//! Note that you need to specify which kind of object you want in return.
#![cfg_attr(
    not(feature = "chrono"),
    doc = "Check out the feature `chrono` to convert to `NaiveDate`."
)]
#![cfg_attr(
    feature = "chrono",
    doc = "
For example, you can also get a `NaiveDate` from the `chrono` crate (with
`chrono` feature).
```
# use chrono::NaiveDate;
# use public_holiday::{Date, FromDate, PublicHoliday};
# #[derive(Debug, PartialEq)]
# enum FrancePublicHoliday { NewYear }
# impl PublicHoliday for FrancePublicHoliday {
#   fn from_ymd(year: i32, month: u32, day: u32) -> Option<Self> {
#     if month == 1 && day == 1 {
#       Some(FrancePublicHoliday::NewYear)
#     } else {
#       None
#     }
#   }
#   fn in_year<D: FromDate>(&self, year: i32) -> Option<D> {
#     if *self == FrancePublicHoliday::NewYear {
#       Some(D::from_ymd(year, 1, 1))
#     } else {
#       None
#     }
#   }
# }
let date = FrancePublicHoliday::NewYear.in_year::<NaiveDate>(1970).unwrap();
assert_eq!(date, NaiveDate::from_ymd(1970, 1, 1));
```
"
)]

mod date;
pub use date::Date;

pub mod weekday;
pub use weekday::Weekday;

#[cfg(feature = "chrono")]
mod chrono;

pub mod regions;

#[cfg(feature = "easter")]
pub mod easter;
mod utils;

#[cfg(test)]
mod test_holiday;

/// Trait for converting from a date
pub trait FromDate {
    /// Convertion into any type a date (from year, month and day)
    fn from_ymd(year: i32, month: u32, day: u32) -> Self;
}

/// Trait to find out about public holidays
pub trait PublicHoliday: Sized {
    /// Find which public holiday is the input date.
    fn from_ymd(year: i32, month: u32, day: u32) -> Option<Self>;
    /// Returns the date of a public holiday for a specified year.
    fn in_year<D>(&self, year: i32) -> Option<D>
    where
        D: FromDate;
}

/// Trait implemented by types representing date to find if the current date is
/// a public holiday.
trait IsHoliday {
    /// Returns which public holiday is the date of `self`.
    fn is_holiday<H>(&self) -> Option<H>
    where
        H: PublicHoliday;
}
