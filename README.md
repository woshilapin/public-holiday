[![Crates.io](https://img.shields.io/crates/v/public-holiday)](https://crates.io/crates/public-holiday)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/woshilapin/public-holiday)](https://gitlab.com/woshilapin/public-holiday/pipelines)
[![License GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue)](https://www.gnu.org/licenses/gpl-3.0.en.html)

Public Holiday in Rust
======================
This Rust crate will help you with public holidays in different countries.

The two main functionalities provided are:
- Find out if a specific date is a public holiday
- Find what is the date of a specific public holiday for a specific year

## Is it a public holiday?
Imagine, you have a date (for example, 22nd of April, 1935), and you want to
know if it is a public holiday in United Kingdom.

```rust
use jiaqi::{PublicHoliday, UKHoliday};

fn main() {
	assert_eq!(UKHoliday.from_ymd(1935, 4, 22), Some(UKHoliday::EasterMonday));
}
```
Yes it was a public holiday. And even better, you know it was Easter Monday.

## When is this public holiday?
Now, let say you want to know which day was Easter Monday in 1907 in France.

```rust
use jiaqi::{PublicHoliday, Date, FRHoliday};

fn main() {
	let date = FRHoliday::LundiDePaques::<Date>(1907).unwrap();
	assert_eq!(date.0, 1907);
	assert_eq!(date.1, 4);
	assert_eq!(date.2, 1);
}
```
Easter Monday was the 1st of April in 1907!

## `Date` is really too dumb!
`jiaqi::Date` is a really simple struct that might be too simple for your needs.
Good news, it's really easy to support other types. For example, the crate
`chrono` is already supported (with the feature `chrono`).

```rust
use chrono::{Datelike, NaiveDate};
use jiaqi::{PublicHoliday, FRHoliday};

fn main() {
	let date = FRHoliday::LundiDePaques::<NaiveDate>(1907).unwrap();
	assert_eq!(date.year(), 1907);
	assert_eq!(date.month(), 4);
	assert_eq!(date.day(), 1);
}
```

Even better, instead of using `UKHoliday::from_ymd(...)`, you can directly use
`NaiveDate::is_holiday()`.

```rust
use chrono::NaiveDate;
use jiaqi::{IsHoliday, UKHoliday};

fn main() {
	let date = NaiveDate::from_ymd(1935, 4, 22);
	assert_eq!(date.is_holiday(), Some(UKHoliday::EasterMonday));
}
```
